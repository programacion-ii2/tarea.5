/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.point;

/**
 *
 * @author USUARIO
 */



import java.io.ObjectInputStream;
import java.util.Scanner;
public class Point {
    Integer x;
    Integer y;
    
    
    
    public Point(Integer x, Integer y){
        this.x = x;
        this.y = y;
    }
    
    
    
    public Integer getX(){
        return this.x;
    } 
    
    public void setX(Integer x){
        this.x = x;
    }
    
    public Integer getY(){
        return this.y;
    }
    
    public void setY(Integer y){
        this.y = y;
    }
    
    public String getData(){
        return "X = " +this.getX() + "\nY =" + this.getY();
        
    }
    
    public Point sumaPuntos(Point a, Point b){
        int sumaX = a.getX() + b.getX();
        int sumaY = a.getY() + b.getY();
        
        return new Point(sumaX, sumaY);
    }
    
    public static Point sumaPuntos(Point ...points){
        int sumaX = 0;
        int sumaY = 0;
        for(Point point : points){
            sumaX += point.getX(); 
            sumaY += point.getY();
        }
        return new Point(sumaX, sumaY);
    }
    public Point restarPuntos(Point a, Point b ){
        int restarX = a.getX() - b.getX();
        int restarY = a.getY() - b.getY();
        
        return new Point(restarX, restarY);
    
    }
    
     public static Point restarPuntos(Point ...points){
        int restarX = 0;
        int restarY = 0;
        for(Point point : points){
            restarX -= point.getX(); 
            restarY -= point.getY();
        }
        return new Point( restarX, restarY);
     
     }
     
     public Point multiplicarPuntos(Point a, Point b ){
        int multiplicarX = a.getX() * b.getX();
        int multiplicarY = a.getY() * b.getY();
        
        return new Point(multiplicarX, multiplicarY);
    
    }
    
     public static Point multiplicarPuntos(Point ...points){
        int multiplicarX = 1;
        int multiplicarY = 1;
        for(Point point : points){
            multiplicarX *= point.getX(); 
            multiplicarY *= point.getY();
        }
        return new Point( multiplicarX, multiplicarY);
     
     }
     
     
     
    
}




